const mongoose = require('mongoose')
const User = require('./user.js')
const Pokemon = require('./pokemon.js');

const uri = "mongodb+srv://dbUser:dbUserPassword@cluster0.sjdx3.gcp.mongodb.net/homenet?retryWrites=true&w=majority"

const connectDb = () => {
  return mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true })
};

module.exports = {
  connectDb,
  models: {
    User,
    Pokemon
  }
} 
const mongoose = require('mongoose');

// User Schema 
const userSchema = new mongoose.Schema({
    username: String,
    password: String,
    score: Number,
    spotted: [{
        id: Number,
        name: String,
        imgUrl: String,
        latitude: String,
        longitude: String,
        gender: String,
        shiny: Boolean,
        date: Date
    }],
    
}) 

const User = mongoose.model('user', userSchema)

module.exports = User;

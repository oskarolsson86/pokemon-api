const mongoose = require('mongoose');

const pokeSchema = new mongoose.Schema({
    id: Number,
    spotted: Number
});

const Pokemon = mongoose.model('pokemon', pokeSchema);

module.exports = Pokemon;
const express = require('express');
const cors = require('cors');
const fetch = require('node-fetch')
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

const routes = require("./routes")
const db = require("./models")

dotenv.config()
//Setting secret key to jwt should be more secure....
process.env.ACCESS_TOKEN_SECRET = "föslfjsö";

const app = express();

const port = process.env.PORT || 3000;

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use((error, req, res, next) => {
    if (res.headersSent) {
      return next(err)
    }
    res.status(error.statusCode || error.status || 500).send({error: error })
  })
  
  app.use((req, res, next) => {
    req.models = db.models
    next()
  })

//Checks every request if the user has a valid JWT some endpoints are open to users who are not signed in
  app.use((req, res, next) =>  {

    if ( req.url == '/login' || req.url == '/users/toprank' || req.url == '/users' || req.url == "/rarest-pokemons") {
      
      next()
    } else {
        const authHeader = req.headers['token']
        if (authHeader === null) return res.sendStatus(401) 
      
        jwt.verify(authHeader, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
          console.log(user)
          if (err) return res.sendStatus(403)
          req.user = user
          next() 
        })
      
    }
   
    
  })

  app.use('/', routes)

  // Start up the database, then the server and begin listen to requests
if(process.env.NODE_ENV != "test") {
    db.connectDb().then(() => {
      const listener = app.listen(port, () => {
        console.info(`Server is listening on port ${listener.address().port}.`);
      })
    }).catch((error) => {
      console.error(error)
    })
  }
  
  module.exports = app
//Get all pokemons
get = (req, res, next) => {
    req.models.Pokemon.find()
    .then( (pokemons) => {
      return res.status(200).send(pokemons)
    }).catch((error) => {
      next(error)
    })
  }

  // Get all spotted pokemons
getSpottedPokemons = (req, res, next) => {
    req.models.Pokemon.find({spotted: { $gte: 1 }})
    .then( pokemons => {
        return res.status(200).send(pokemons)
    })
    .catch( err => {
        next(err)
    })
}

// get all unspotted pokemons
getUnspottedPokemons = (req, res, next) => {
    req.models.Pokemon.find({spotted: { $lt: 1 }})
    .then( pokemons => {
        return res.status(200).send(pokemons)
    })
    .catch( err => {
        next(err)
    })
}

// Get the rarest pokemons
getRarestPokemons = (req, res, next) => {
    req.models.Pokemon.find().sort({spotted: 1}).limit(10)
      .then( rarePokes => {
        return res.status(200).send(rarePokes)
      })
      .catch( err => {
        next(err)
      })
}

  module.exports = {
    get,
    getSpottedPokemons,
    getUnspottedPokemons,
    getRarestPokemons
}
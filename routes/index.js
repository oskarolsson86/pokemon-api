const express = require('express')
const router = express.Router()

// All endpoints to the api

const user = require('./user.js')
const pokemon = require('./pokemon.js')

router.post("/login", user.login)

router.get("/users", user.get)
router.get("/users/toprank", user.getTopTen)
router.post("/users", user.post)
router.put("/users", user.put)

router.get("/pokemons", pokemon.get);
router.get("/spotted-pokemons", pokemon.getSpottedPokemons)
router.get("/unspotted-pokemons", pokemon.getUnspottedPokemons)
router.get("/rarest-pokemons", pokemon.getRarestPokemons)

module.exports = router
const jwt = require("jsonwebtoken");

// Login user and returns the JWT if signed in.
login = (req, res, next) => {

  let reqUsername = req.body.username;
  let reqPassword = req.body.password;

  req.models.User.find({username: reqUsername, password: reqPassword})
    .then( user => {

      if (user.length > 0) {
        let returnToken = { token: generateAccessToken({username: reqUsername})}
      return res.status(200).send(returnToken)
      } else {
        return res.status(400).send({fail : "failll"})
      }
      
      
    })
}

//Get all users or a single user based on 
get = (req, res, next) => {
    let token = req.headers['token'];
    let usernameFromToken = jwt.decode(token);
    var query;
    if(req.query.username) {
      query = req.models.User.find({username: usernameFromToken.username}, 'score password username spotted')
    }
    else
    {
      query = req.models.User.find({}, 'score username spotted')
    }
  
    query.exec().then((user) => {
      
        return res.send(user);
      }).catch((error) => {
        next(error)
      })
  }

  // returns top ten ranked users based on their score. It was supposed to do this bu now we return a sorted ranking of all users
  getTopTen = (req, res, next) => {
    req.models.User.find({}, 'score username spotted').sort({score: -1})
    .then( (user) => {
      return res.status(200).send(user)
    }).catch((error) => {
      next(error)
    })
  }

  function generateAccessToken(username) {
    // expires after half and hour (1800 seconds = 30 minutes)
    return jwt.sign(username, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1800s' });
  }

  // Create new user also check if username is already taken
  post = (req, res, next) => {
    const reqUsername = req.body.username;
    
    req.models.User.find({username: reqUsername}, (err, user) => {
      if (user.length > 0) {
        return res.status(409).send({error : "User already exists"})
      } else {
          req.models.User.create({
            score: 0,
            username: req.body.username,
            password: req.body.password,
            spotted: req.body.spotted,
          }).then( user => {
         
            return res.status(201).send(user)
          }).catch(err => {
            console.error(err)
          })
      }
    }
    )
  }

  // push a spotted pokemon to the users spotted array. Increment the score depending if it's shiny or not.
  put = (req, res, next) => {

    let token = req.headers['token'];
    let usernameFromToken = jwt.decode(token);

    req.models.Pokemon.findOneAndUpdate({id: req.body.spotted.id}, { $inc: {spotted: +1} })
        .then(pokemon => {
            return res.status(200).send(pokemon)
        })
        .catch( err => {
            next(err)
        }) 

    req.models.User.updateMany({username: usernameFromToken.username},

      {
        $inc: { score: req.body.spotted.shiny ?  + 2 : + 1},
        $push: {
        spotted: {
            id: req.body.spotted.id,
            name: req.body.spotted.name,
            latitude: req.body.spotted.latitude,
            longitude: req.body.spotted.longitude,
            gender: req.body.spotted.gender,
            shiny: req.body.spotted.shiny,
            date: req.body.spotted.date
        }}
       
      },
      {
        new: true,
        upsert: true,
        runvalidators: true,
      }).then((status) => {
        console.log("status: ", status)
        if (status.upserted)
          res.status(201)
        else if (status.nModified)
          res.status(200)
        else
          res.status(204)
      res.send()
      }).catch((error) => next(error))

      
  }

  module.exports = {
      get,
      post,
      put,
      getTopTen,
      login
  }
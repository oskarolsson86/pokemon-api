# Oskar Olsson, Tora Haukka, Andreas Lif

>**GET**
>
>/users
>
>retrieves all users


> **GET**
> 
>/users?username=[NAME]
>
>    retrieves one specific user based on username in the JWT

>**GET**
>
>/users/toprank
>
>retrieves top ten users based on their score

>**POST**
>
>/users
>
>creates new User
>
> ### Data structure to create new User
> ```
>{
>    "username": "oskar",
>    "password": "1234",
>    "spotted": []
>}
> ```


> **POST**
> 
> /login
>
> ### Data structure for login
> ```
> { 
>   username: string,
>   password: supersecretpassword
> }
> ```
> 

>**PUT**
>
>/users
>
>pushes spotted pokemon to the users spotted array. gets the username from the jwt 
>
> ### Data structure to add new spotted pokemon
> ```
> {
>    "spotted": {
>        "id": 1,
>        "name": "SvenPokemon",
>        "imgUrl": "www.somepictueidontknw.com",
>        "latitude": "425424255",
>        "longitude": "5424242325",
>        "gender": "female",
>        "shiny": false,
>        "date": "2012-04-23T18:25:43.511Z" // DATE FORMAT
>    }
>}
> ```

### Pokemon

>**GET**
>
>/spotted-pokemons
>
>retrieves all spotted pokemons

>**GET**
>
>/unspotted-pokemons
>
>retrieves all unspotted pokemons

>**GET**
>
>/rarest-pokemons
>
>retrieves the 10 most uncommon pokemons